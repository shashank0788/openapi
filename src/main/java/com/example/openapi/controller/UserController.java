package com.example.openapi.controller;

import com.example.openapi.api.UserApiDelegate;
import com.example.openapi.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
@RestController
public class UserController implements UserApiDelegate {
    @Override
    public ResponseEntity<User> getUser() {
        User user = new User();
        user.setId(new BigDecimal(123));
        user.setName("Petros");
        user.setAge(new BigDecimal(35));
        return ResponseEntity.ok(user);
    }
}